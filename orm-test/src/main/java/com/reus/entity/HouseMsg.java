package com.reus.entity;

import com.reus.annotation.Column;
import com.reus.annotation.Id;
import com.reus.annotation.Table;
import lombok.Data;

import java.util.Date;

/**
 * description:
 * copyright: Copyright (c) 2018-2021
 * company: iSysCore Tech. Co., Ltd.
 *
 * @author liuxq@isyscore.com
 * @version 1.0
 * @date 2021-10-15 17:39:53
 */
@Data
@Table(name = "house_msg")
public class HouseMsg {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "msg")
    private String msg;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "agent_id")
    private Integer agentId;

    @Column(name = "house_id")
    private Integer houseId;

    @Column(name = "user_name")
    private String  userName;
}
