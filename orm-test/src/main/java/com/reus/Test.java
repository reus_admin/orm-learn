package com.reus;

import com.reus.config.Config;
import com.reus.config.Session;
import com.reus.config.SessionFactory;
import com.reus.config.SessionFactoryBuilder;
import com.reus.entity.HouseMsg;

import java.util.Date;

/**
 * description:
 * copyright: Copyright (c) 2018-2021
 * company: iSysCore Tech. Co., Ltd.
 *
 * @author liuxq@isyscore.com
 * @version 1.0
 * @date 2021-10-15 18:09:17
 */
public class Test {
    public static void main(String[] args) throws Exception {
        testSave();
        testFindOne();
        testDelete();
    }

    public static void testSave() throws Exception {
        Config config = new Config();
        SessionFactory factory = SessionFactoryBuilder.build(config);
        Session session = factory.createSession();
        HouseMsg houseMsg = new HouseMsg();
        houseMsg.setId(1112);
        houseMsg.setAgentId(100);
        houseMsg.setCreateTime(new Date());
        houseMsg.setHouseId(100);
        houseMsg.setMsg("测试");
        houseMsg.setUserName("小明");
        session.save(houseMsg);

    }

    public static void testDelete() throws Exception {
        Config config = new Config();
        SessionFactory factory = SessionFactoryBuilder.build(config);
        Session session = factory.createSession();
        HouseMsg houseMsg = new HouseMsg();
        houseMsg.setId(1112);
        session.delete(houseMsg);

    }

    public static void testFindOne() throws Exception {
        Config config = new Config();
        SessionFactory factory = SessionFactoryBuilder.build(config);
        Session session = factory.createSession();
        HouseMsg one = (HouseMsg) session.findOne(HouseMsg.class, 1112);
        System.out.println(one);

    }
}
