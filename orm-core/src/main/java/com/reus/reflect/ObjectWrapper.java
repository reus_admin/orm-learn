package com.reus.reflect;

import java.util.Set;

/**
 * description: 对象增强,封装了get,set方法
 * copyright: Copyright (c) 2018-2021
 * company: iSysCore Tech. Co., Ltd.
 *
 * @author liuxq@isyscore.com
 * @version 1.0
 * @date 2021-10-19 19:22:25
 */
public class ObjectWrapper {
    // 真实对象
    private Object realObject;
    // 该对象的类的增强
    private ClazzWrapper clazzWrapper;

    public ObjectWrapper(Object realObject) {
        this.realObject = realObject;
        this.clazzWrapper = new ClazzWrapper(realObject.getClass());
    }

    // 调用对象指定属性的get方法
    public Object getVal(String property) throws Exception {

        return clazzWrapper.getGetterMethod(property).invoke(realObject, null);
    }

    // 调用对象指定属性的set方法
    public void setVal(String property, Object value) throws Exception {

        clazzWrapper.getSetterMethod(property).invoke(realObject, value);
    }

    public Set<String> getProperties() {

        return clazzWrapper.getProperties();
    }

    public Set<ClazzWrapper.FiledExpand> getMapperFiledExpands() {

        return clazzWrapper.getFiledExpandSet();
    }

    public Object getRealObject() {

        return realObject;

    }
}
