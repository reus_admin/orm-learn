package com.reus.reflect;

/**
 * description: 对象增强类。使用对象增强是因为我们不知道每个对象的getXXX方法，只能利用反射获取对象的属性和方法，然后
 * 统一提供getVal或者setVal来获取或者设置指定属性，这就是对象的增强的重要性
 * copyright: Copyright (c) 2018-2021
 * company: iSysCore Tech. Co., Ltd.
 *
 * @author liuxq@isyscore.com
 * @version 1.0
 * @date 2021-10-19 19:22:59
 */
public class ObjectWrapperFactory {

    public static ObjectWrapper getInstance(Object o) {
        return new ObjectWrapper(o);
    }

}
