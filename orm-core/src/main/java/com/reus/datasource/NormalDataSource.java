package com.reus.datasource;

import com.reus.driver.DriverRegister;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

/**
 * description:
 * copyright: Copyright (c) 2018-2021
 * company: iSysCore Tech. Co., Ltd.
 *
 * @author liuxq@isyscore.com
 * @version 1.0
 * @date 2021-10-18 16:55:57
 */
public class NormalDataSource implements DataSource {

    /**
     * description: 驱动名称
     *
     * @modified liuxq@isyscore.com
     * @date 2021-10-18
     */
    private String driverClassName;

    /**
     * description: 数据库连接URL
     *
     * @modified liuxq@isyscore.com
     * @date 2021-10-18
     */
    private String url;

    /**
     * description: 数据库用户名
     *
     * @modified liuxq@isyscore.com
     * @date 2021-10-18
     */
    private String userName;

    /**
     * description: 数据库密码
     *
     * @modified liuxq@isyscore.com
     * @date 2021-10-18
     */
    private String passWord;

    /**
     * description: 驱动注册中心
     *
     * @modified liuxq@isyscore.com
     * @date 2021-10-18
     */
    private DriverRegister driverRegister = new DriverRegister();

    public NormalDataSource(String driverClassName, String url, String userName, String passWord) {
        // 初始化时将驱动进行注册
        this.driverRegister.loadDriver(driverClassName);
        this.driverClassName = driverClassName;
        this.url = url;
        this.userName = userName;
        this.passWord = passWord;
    }

    /**
     * description: 获取数据库连接
     *
     * @modified liuxq@isyscore.com
     * @date 2021-10-18
     * @return: java.sql.Connection
     */
    @Override
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, userName, passWord);

    }

    /**
     * description: 移除数据库连接，此方法没有真正移除，只是将连接中未提交的事务进行回滚操作。
     *
     * @param connection:
     * @modified liuxq@isyscore.com
     * @date 2021-10-18
     * @return: void
     */
    public void removeConnection(Connection connection) throws SQLException {
        if (!connection.getAutoCommit()) {
            connection.rollback();
        }
    }

    /**
     * description: 获取数据库连接
     *
     * @param username:
     * @param password:
     * @modified liuxq@isyscore.com
     * @date 2021-10-18
     * @return: java.sql.Connection
     */
    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }

    /**
     * description: 后续方法因为没有用到，所有没有进行重写了
     *
     * @param iFace:
     * @modified liuxq@isyscore.com
     * @date 2021-10-18
     * @return: T
     */
    @Override
    public <T> T unwrap(Class<T> iFace) throws SQLException {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iFace) throws SQLException {
        return false;
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return null;
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {

    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {

    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return 0;
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return null;
    }
}
