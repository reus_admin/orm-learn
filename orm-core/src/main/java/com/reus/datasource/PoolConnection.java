package com.reus.datasource;

import java.sql.Connection;

/**
 * description: 连接代理类，是对Connection的一个封装。除了提供基本的连接外，还想记录
 * 这个连接的连接时间，因为有的连接如果一直连接不释放，那木我可以通过查看
 * 这个连接已连接的时间，如果超时了，我可以主动释放。
 * copyright: Copyright (c) 2018-2021
 * company: iSysCore Tech. Co., Ltd.
 *
 * @author liuxq@isyscore.com
 * @version 1.0
 * @date 2021-10-18 16:53:54
 */
public class PoolConnection {

    /**
     * description: 真实的数据库连接
     *
     * @modified liuxq@isyscore.com
     * @date 2021-10-18
     */
    public Connection connection;

    /**
     * description: 数据开始连接时间
     *
     * @modified liuxq@isyscore.com
     * @date 2021-10-18
     */
    private Long CheckOutTime;

    /**
     * description: 连接的hashCode
     *
     * @modified liuxq@isyscore.com
     * @date 2021-10-18
     */
    private int hashCode = 0;

    public PoolConnection(Connection connection) {
        this.connection = connection;
        this.hashCode = connection.hashCode();
    }

    public Long getCheckOutTime() {
        return CheckOutTime;
    }

    public void setCheckOutTime(Long checkOutTime) {
        CheckOutTime = checkOutTime;
    }

    /**
     * description: 判断两个PoolConnection对象是否相等，其实是判断其中真实连接的hashCode是否相等
     *
     * @param obj:
     * @modified liuxq@isyscore.com
     * @date 2021-10-18
     * @return: boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PoolConnection) {
            return connection.hashCode() ==
                    ((PoolConnection) obj).connection.hashCode();
        } else if (obj instanceof Connection) {
            return obj.hashCode() == hashCode;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return hashCode;
    }
}
