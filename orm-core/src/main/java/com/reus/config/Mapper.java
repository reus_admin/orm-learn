package com.reus.config;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * description: 映射模型
 * copyright: Copyright (c) 2018-2021
 * company: iSysCore Tech. Co., Ltd.
 *
 * @author liuxq@isyscore.com
 * @version 1.0
 * @date 2021-10-15 17:45:24
 */
@Setter
@Getter
public class Mapper {

    /**
     * 类名称
     */
    private String className;

    /**
     * 表名称
     */
    private String tableName;

    /**
     * 主键的映射信息
     */
    private Map<String, String> primaryKeyMapping;

    /**
     * 列的映射信息
     */
    private Map<String, String> columnMapping;
}

