package com.reus.config;

/**
 * description: 用于创建工厂对象
 * copyright: Copyright (c) 2018-2021
 * company: iSysCore Tech. Co., Ltd.
 *
 * @author liuxq@isyscore.com
 * @version 1.0
 * @date 2021-10-15 17:56:46
 */
public class SessionFactoryBuilder {

    private static SessionFactory sessionFactory;

    private SessionFactoryBuilder() {
    }

    public static synchronized SessionFactory build(Config config) {
        if (sessionFactory != null) {
            return sessionFactory;
        }
        return new SessionFactory(config);
    }

}
