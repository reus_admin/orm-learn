package com.reus.util;

import com.reus.annotation.Column;
import com.reus.annotation.Id;
import com.reus.annotation.Table;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * description: 使用反射解析实体类中注解的工具类
 * copyright: Copyright (c) 2018-2021
 * company: iSysCore Tech. Co., Ltd.
 *
 * @author liuxq@isyscore.com
 * @version 1.0
 * @date 2021-10-15 17:41:20
 */
@Slf4j
public class AnnotationUtil {

    /**
     * description: 得到的类名
     *
     * @param clz:
     * @modified liuxq@isyscore.com
     * @date 2021-10-15
     * @return: java.lang.String
     */
    public static String getClassName(Class clz) {
        return clz.getName();
    }

    /**
     * description: 得到Table注解中的表名
     *
     * @param clz:
     * @modified liuxq@isyscore.com
     * @date 2021-10-15
     * @return: java.lang.String
     */
    public static String getTableName(Class clz) {
        if (clz.isAnnotationPresent(Table.class)) {
            Table Table = (Table) clz.getAnnotation(Table.class);
            return Table.name();
        } else {
            log.error("缺少Table注解:{}", clz.getName());
            return null;
        }
    }

    /**
     * description: 得到主键属性和对应的字段
     *
     * @param clz:
     * @modified liuxq@isyscore.com
     * @date 2021-10-15
     * @return: java.util.Map<java.lang.String, java.lang.String>
     */
    public static Map<String, String> getIdMapper(Class clz) {
        boolean flag = true;
        Map<String, String> map = new HashMap();
        Field[] fields = clz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Id.class)) {
                flag = false;
                String fieldName = field.getName();
                if (field.isAnnotationPresent(Column.class)) {
                    Column Column = field.getAnnotation(Column.class);
                    String columnName = Column.name();
                    map.put(fieldName, columnName);
                    break;
                } else {
                    log.error("缺少Column注解:{}", field.getName());
                }
            }
        }
        if (flag) {
            log.error("缺少Id注解:{}", clz.getName());
        }
        return map;
    }

    /**
     * description: 得到类中所有属性和对应的字段
     *
     * @param clz:
     * @modified liuxq@isyscore.com
     * @date 2021-10-15
     * @return: java.util.Map<java.lang.String, java.lang.String>
     */
    public static Map<String, String> getPropMapping(Class clz) {
        Map<String, String> map = new HashMap();
        map.putAll(getIdMapper(clz));
        Field[] fields = clz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Column.class)) {
                Column Column = field.getAnnotation(Column.class);
                String fieldName = field.getName();
                String columnName = Column.name();
                map.put(fieldName, columnName);
            }
        }
        return map;
    }

    /**
     * description: 获得某包下面的所有类名
     *
     * @param packagePath:
     * @modified liuxq@isyscore.com
     * @date 2021-10-15
     * @return: java.util.Set<java.lang.String>
     */
    public static Set<String> getClassNameByPackage(String packagePath) {
        Set<String> names = new HashSet();
        String packageFile = packagePath.replace(".", "/");
        String classpath = new Thread().getContextClassLoader().getResource("").getPath();
        if (classpath == null) {
            classpath = Thread.currentThread().getContextClassLoader().getResource("/").getPath();
        }
        try {
            classpath = URLDecoder.decode(classpath, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        File dir = new File(classpath + packageFile);
        if (dir.exists()) {
            File[] files = dir.listFiles();
            for (File f : files) {
                String name = f.getName();
                if (f.isFile() && name.endsWith(".class")) {
                    name = packagePath + "." + name.substring(0, name.lastIndexOf("."));
                    names.add(name);
                }
            }
        } else {
            log.error("包路径不存在:{}", packagePath);
        }
        return names;
    }
}
