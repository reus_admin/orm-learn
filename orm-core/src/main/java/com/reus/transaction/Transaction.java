package com.reus.transaction;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * description: 封装事务功能
 * copyright: Copyright (c) 2018-2021
 * company: iSysCore Tech. Co., Ltd.
 *
 * @author liuxq@isyscore.com
 * @version 1.0
 * @date 2021-10-19 19:14:13
 */
public interface Transaction {

    /**
     * 获取数据库连接
     */
    Connection getConnection() throws SQLException;

    /**
     * 事务提交
     */
    void commit() throws SQLException;

    /**
     * 事务回滚
     */
    void rollback() throws SQLException;

    /**
     * 关闭连接
     */
    void close() throws SQLException;
}
