package com.reus.transaction;

import com.reus.datasource.PoolDataSource;

/**
 * description: 事务工厂，封装获取事务的功能
 * copyright: Copyright (c) 2018-2021
 * company: iSysCore Tech. Co., Ltd.
 *
 * @author liuxq@isyscore.com
 * @version 1.0
 * @date 2021-10-19 19:15:35
 */
public class TransactionFactory {

    public static SimpleTransaction newTransaction(PoolDataSource poolDataSource, Integer level, Boolean autoCommmit) {
        return new SimpleTransaction(poolDataSource, level, autoCommmit);
    }

}
