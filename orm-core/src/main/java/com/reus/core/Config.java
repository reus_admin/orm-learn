package com.reus.core;

import com.reus.datasource.PoolDataSource;

/**
 * description: 框架的核心配置类
 * copyright: Copyright (c) 2018-2021
 * company: iSysCore Tech. Co., Ltd.
 *
 * @author liuxq@isyscore.com
 * @version 1.0
 * @date 2021-10-19 19:08:17
 */
public class Config {

    // 数据源
    private PoolDataSource dataSource;

    // mapper包地址（即待解析的包名），后续改为List<String>，能同时加载多个mapper包 todo
    private String daoSource;

    // mapper核心文件（存放类解析后的对象）
    private MapperCore mapperCore;

    // 是否启用事务
    private boolean openTransaction;

    // 是否开启缓存
    private boolean openCache;

    public Config(String mapperSource,PoolDataSource dataSource){
        this.dataSource = dataSource;
        this.daoSource = mapperSource;
        this.mapperCore = new MapperCore(this);
    }

    // 生成SQL语句执行器
//    public SimpleExecutor getExecutor(){
//        return new SimpleExecutor(this,this.getDataSource(),openTransaction,openCache);
//    }

    // 后文都是基本的get,set方法
    public PoolDataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(PoolDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public String getDaoSource() {
        return daoSource;
    }

    public void setDaoSource(String daoSource) {
        this.daoSource = daoSource;
    }

    public MapperCore getMapperCore() {
        return mapperCore;
    }

    public void setMapperCore(MapperCore mapperCore) {
        this.mapperCore = mapperCore;
    }

    public boolean isOpenTransaction() {
        return openTransaction;
    }

    public void setOpenTransaction(boolean openTransaction) {
        this.openTransaction = openTransaction;
    }

    public boolean isOpenCache() {
        return openCache;
    }

    public void setOpenCache(boolean openCache) {
        this.openCache = openCache;
    }

}
