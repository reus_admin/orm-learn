package com.reus.cache;

/**
 * description: 自定义缓存接口,主要提供增删改查功能
 * copyright: Copyright (c) 2018-2021
 * company: iSysCore Tech. Co., Ltd.
 *
 * @author liuxq@isyscore.com
 * @version 1.0
 * @date 2021-10-19 19:11:59
 */
public interface Cache {

    /**
     * 放入缓存
     */
    void putCache(String key, Object val);

    /**
     * 获取缓存
     */
    Object getCache(String key);

    /**
     * 清空缓存
     */
    void cleanCache();

    /**
     * 获取缓存健数量
     */
    int getSize();

    /**
     * 移除key的缓存
     */
    void removeCache(String key);
}
