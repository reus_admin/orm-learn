package com.reus.cache;

import java.util.HashMap;
import java.util.Map;

/**
 * description: 缓存的简单实现
 * copyright: Copyright (c) 2018-2021
 * company: iSysCore Tech. Co., Ltd.
 *
 * @author liuxq@isyscore.com
 * @version 1.0
 * @date 2021-10-19 19:12:29
 */
public class SimpleCache implements Cache {

    /**
     * 用HashMap来实现缓存的增删改查
     */
    private static Map<String, Object> map = new HashMap<>();


    @Override
    public void putCache(String key, Object val) {
        map.put(key, val);
    }

    @Override
    public Object getCache(String key) {
        return map.get(key);
    }

    @Override
    public void cleanCache() {
        map.clear();
    }

    @Override
    public int getSize() {
        return map.size();
    }

    @Override
    public void removeCache(String key) {
        map.remove(key);
    }
}
