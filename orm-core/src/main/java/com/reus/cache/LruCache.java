package com.reus.cache;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * description: 缓存包装类没，使缓存具有LRU功能
 * copyright: Copyright (c) 2018-2021
 * company: iSysCore Tech. Co., Ltd.
 *
 * @author liuxq@isyscore.com
 * @version 1.0
 * @date 2021-10-19 19:13:07
 */
public class LruCache implements Cache {

    /**
     * 缓存大小
     */
    private static Integer cacheSize = 2000;

    /**
     * HashMap负载因子，可以自己指定
     */
    private static Float loadFactory = 0.75F;

    /**
     * 真实缓存
     */
    private Cache trueCache;

    /**
     * 使用LinkedHashMap来实现Lru功能
     */
    private Map<String, Object> linkedCache;

    /**
     * 待移除的元素
     */
    private static Map.Entry removeEntry;

    public LruCache(Cache trueCache) {
        this(cacheSize, loadFactory, trueCache);
    }

    public LruCache(Integer cacheSize, Float loadFactory, Cache trueCache) {
        this.trueCache = trueCache;
        /**初始化LinkedHashMap,并重写removeEldestEntry() 方法,不了解的可以看我之前的博客*/
        this.linkedCache = new LinkedHashMap<String, Object>(cacheSize, loadFactory, true) {
            @Override
            protected boolean removeEldestEntry(Map.Entry eldest) {
                // 当缓存容量超过设置的容量时，返回true,并记录待删除的对象
                if (getSize() > cacheSize) {
                    removeEntry = eldest;
                    return true;
                }
                return false;
            }
        };
    }

    // 放入缓存，当待删除的元素不为空时，就在真实缓存中删除该元素
    @Override
    public void putCache(String key, Object val) {
        // 真实缓存中放入
        this.trueCache.putCache(key, val);
        // linkedHashMap中放入,此处放入会调用重写的removeEldestEntry()方法
        this.linkedCache.put(key, val);
        // 当removeEldestEntry()执行后，如果有待删除元素，就开始执行删除操作
        if (removeEntry != null) {
            removeCache((String) removeEntry.getKey());
            removeEntry = null;
        }
    }

    @Override
    public Object getCache(String key) {
        // 因为调用linkedHashMap中的get()方法会对链表进行再一次排序。见之前关于缓存的博客
        linkedCache.get(key);
        return trueCache.getCache(key);
    }

    @Override
    public void cleanCache() {
        trueCache.cleanCache();
        linkedCache.clear();
    }

    @Override
    public int getSize() {
        return trueCache.getSize();
    }

    @Override
    public void removeCache(String key) {
        trueCache.removeCache(key);
    }
}
