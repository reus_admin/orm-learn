package com.reus.execute;

import java.sql.SQLException;

/**
 * description: 执行接口类，后续执行器需要实现此接口
 * copyright: Copyright (c) 2018-2021
 * company: iSysCore Tech. Co., Ltd.
 *
 * @author liuxq@isyscore.com
 * @version 1.0
 * @date 2021-10-19 19:16:11
 */
public interface Executor {

    /**
     * 获取Mapper接口代理类，使用了动态代理技术，见实现类分析
     */
    <T> T getMapper(Class<T> type);

    void commit() throws SQLException;

    void rollback() throws SQLException;

    void close() throws SQLException;
}
